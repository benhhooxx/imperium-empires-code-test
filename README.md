# Imperium-empires-code-test
## Folder Structure

```
.
├── src
│   ├── __test__ (Focus on testcases with xxx.test.js)
│   ├── assets 
│   ├── css
│   ├── components
│   ├── App.js
│   └── index.js
├── package-lock.json
└── package.json
```

---
This project was developed by [React.js](https://reactjs.org/). Furthermore, I would like to pickup a new animation skills via this project, and the styles was takecare by [React Spring](https://react-spring.io/). For the testing, this project is use [JEST.js](https://jestjs.io/) as a testing framework.


### Why React.js?

Apart form the below common benefits, React.js is better for me to deal with the state, lifecycle, test issues:
- Speend
- Flexibility
- Performance
- Usability
- Reusable Component

### Why React-spring?

React Spring is a framework to deal with the animation issue that I can foucs more on the business requirement more
 - Easy to use
 - Thorough documentation

### Why JEST.js?

JEST.js is a test framework which easy to use, and it serves below benefits
 - Fast & Safe
 - Easy Mocking
 - Code Coverage
 - Great Exceptions

---


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

### `npm test`