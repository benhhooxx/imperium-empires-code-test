import React from "react";

const MessageBox = ({ children, style }) => {
  return (
    <div className="message-box" style={{ ...style }}>
      <div>{children}</div>
      <div className="message-box--border"></div>
    </div>
  );
};

export default MessageBox;
