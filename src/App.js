import React from "react";
import { Spring } from "react-spring/renderprops";
import VisibilitySensor from "./components/VisibilitySensor";
import MessageBox from "./components/MessageBox";
import Ship from "./assets/ship.png";
import Planet from "./assets/planet.png";
import "./App.scss";

const App = () => {
  return (
    <div className="app">
      <h1>Hello Imperium Empires Team</h1>
      <h3>Welcome to review my techincal test!</h3>
      <div className="container">
        <VisibilitySensor partialVisibility>
          {({ isVisible }) => (
            <Spring
              delay={300}
              to={{
                opacity: isVisible ? 1 : 0,
                transform: isVisible ? "translateY(0)" : "translateY(50px)",
              }}
            >
              {(props) => (
                <MessageBox style={{ ...props }}>
                  <h3 className="mt-0">Ben Ho</h3>
                  <p>
                    Frontend Developer in Hong Kong First Blockchain Logistic
                    Platform
                  </p>
                </MessageBox>
              )}
            </Spring>
          )}
        </VisibilitySensor>
        <p>~Scroll Down~</p>
      </div>
      <div className="container">
        <VisibilitySensor partialVisibility>
          {({ isVisible }) => (
            <div className="game-container">
              <div className="lhs">
                <Spring
                  delay={300}
                  to={{
                    opacity: isVisible ? 1 : 0,
                    transform: isVisible
                      ? "translateX(120px)"
                      : "translateX(-800px)",
                  }}
                >
                  {(props) => (
                    <div style={{ ...props }}>
                      <img src={Ship} />
                    </div>
                  )}
                </Spring>
                <div className="parent">
                  <Spring
                    delay={300}
                    to={{
                      opacity: isVisible ? 1 : 0,
                      transform: isVisible
                        ? "translateY(0)"
                        : "translateY(50px)",
                    }}
                  >
                    {(props) => (
                      <MessageBox style={{ ...props }}>
                        <a
                          href="https://www.google.com"
                          target="_blank"
                          rel="noopener noreferrer"
                          className="mt-0 child"
                        >
                          I am a link🔗
                        </a>
                      </MessageBox>
                    )}
                  </Spring>
                </div>
              </div>
              <div>
                <Spring
                  delay={300}
                  to={{
                    opacity: isVisible ? 1 : 0,
                    transform: isVisible
                      ? "translateX(-120px)"
                      : "translateX(800px)",
                  }}
                >
                  {(props) => (
                    <div style={{ ...props }}>
                      <img src={Planet} />
                    </div>
                  )}
                </Spring>
                <div className="parent">
                  <Spring
                    delay={300}
                    to={{
                      opacity: isVisible ? 1 : 0,
                      transform: isVisible
                        ? "translateY(0)"
                        : "translateY(50px)",
                    }}
                  >
                    {(props) => (
                      <MessageBox style={{ ...props }}>
                        <a
                          href="https://www.google.com"
                          target="_blank"
                          rel="noopener noreferrer"
                          className="mt-0 child"
                        >
                          Explore More🌐
                        </a>
                      </MessageBox>
                    )}
                  </Spring>
                </div>
              </div>
            </div>
          )}
        </VisibilitySensor>
      </div>
    </div>
  );
};
export default App;
